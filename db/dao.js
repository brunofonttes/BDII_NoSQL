
const toOutput = (err, result) => {
    if (err) throw err;
    console.log(JSON.stringify(result));
}
	

function listaClientes(r, conn) {   
	return new Promise((resolve, reject)=>{
		r.db("store").table('customers').run(conn, (err, cursor)=> {
		    if (err) throw err;
		    cursor.toArray((err, result)=> {	    	
		        if (err) reject(err);
		        let resultado = JSON.stringify(result, null, 2);
		        resolve(resultado);
		    }); 
		 }); 
	}) 
    

}

function adicionaCliente(r, conn, cliente) {

    r.db("store").table("customers").insert(cliente).run(conn, toOutput);
}

function excluiCliente(r, conn, cliente) {

}

module.exports = {
    listaClientes,
    adicionaCliente,
    excluiCliente
}