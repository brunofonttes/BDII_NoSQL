const dao = getmodule('db/dao');
module.exports = function (app) {
    return {
        main: async function (request, response) {
            const conn = app.get("conn");
            const r = app.get("r");

            let resultado = await dao.listaClientes(r,conn); 
            resultado = JSON.parse(resultado);
            // resultado = resultado.map((r)=>{return({email:r.email,name:r.name})})
            response.render('view',{resultado:resultado});
        },
        recebedados: function (request, response) {
            const conn = app.get("conn");
            const r = app.get("r");

            let body = "";
            request.on('data', function (data) {
                body += data;
            });
            request.on('end', async function () {
                body = JSON.parse(body)
                dao.adicionaCliente(r,conn,body);
            });
            response.writeHead(200, { 'Content-Type': 'text/html' });
            response.end('post received');
        }
    }
}