const express = require('express');
const app = express();
const http = require('http').Server(app);
const consign = require('consign');
const r = require('rethinkdb')

let options = ComposeAuth();

let conn = null;
r.connect(options).then((connection) => {
    conn = connection;
    app.set('conn', conn);
})

app.set('r',r);
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

global.getmodule = function getmodule(modulePath) {
    var path = require('path');
    return require(path.resolve(modulePath));
}

consign()
    .include('controllers')
    .then('routes')
    .into(app);

var port = process.env.PORT || 3006;

http.listen(port, function () {
    console.log('Servidor rodando em http://localhost:%s', port);
})

function ComposeAuth() {
    // We need to parse the connection string for the deployment
    let parseRethinkdbUrl = require("parse-rethinkdb-url");

    // Now lets get cfenv and ask it to parse the environment variable
    const cfenv = require('cfenv');
    const assert = require('assert');
    const util = require('util');

    // load local VCAP configuration  and service credentials
    let vcapLocal;
    try {
        vcapLocal = require('./vcap-local.json');
    }
    catch (e) {
        console.log(e);
    }
    const appEnvOpts = vcapLocal ? { vcap: vcapLocal } : {};
    const appEnv = cfenv.getAppEnv(appEnvOpts);
    // Within the application environment (appenv) there's a services object
    let services = appEnv.services;
    // The services object is a map named by service so we extract the one for rethinkdb
    let rethinkdb_services = services["compose-for-rethinkdb"];
    // This check ensures there is a services for rethinkdb databases
    assert(!util.isUndefined(rethinkdb_services), "Must be bound to compose-for-rethinkdb services");
    // We now take the first bound rethinkdb service and extract it's credentials object
    let credentials = rethinkdb_services[0].credentials;
    let options = parseRethinkdbUrl(credentials.uri);
    let connection;
    // Within the credentials, an entry ca_certificate_base64 contains the SSL pinning key
    // We convert that from a string into a Buffer entry in an array which we use when
    // connecting.
    let caCert = new Buffer(credentials.ca_certificate_base64, 'base64');
    // Now we can insert the SSL credentials
    options.ssl = {
        ca: caCert
    };
    return options;
}

